﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Albireo.Otp;

namespace HOTPDemoApp
{
    public partial class Form1 : Form
    {
        private byte[] secretKeyBytes;
        private int timerCounter = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            InitialLayout();

        }

        private void InitialLayout()
        {
            txtCounter.Text = "1";
            tmrGetHotp.Interval = 1 * 1000;
            tmrGetHotpByChange.Interval = 3 * 1000; // 3 secs buffer to trigger GetHotp()

            secretKeyBytes = new byte[] { 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x21, 0xDE, 0xAD, 0xBE, 0xEF };
            txtSecret.Text = ConvertBytesToHex(secretKeyBytes);

            GetHotp();

            tmrGetHotpByChange.Stop();
            tmrGetHotp.Start();
        }

        private void GetHotp()
        {
            var secretKey = txtSecret.Text;

            if (String.IsNullOrEmpty(secretKey) || String.IsNullOrEmpty(txtCounter.Text))
            {
                return;
            }

            var counter = Convert.ToInt32(txtCounter.Text);

            Console.WriteLine(Albireo.Otp.Hotp.GetCode(HashAlgorithm.Sha1, secretKey, counter));
        }

        /// <summary>
        /// 轉換位元組資料為 16 進位字串表示
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private string ConvertBytesToHex(byte[] bytes)
        {
            string hexString = string.Empty;
            if (bytes != null)
            {
                var stringBuilder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    stringBuilder.Append(bytes[i].ToString("X2"));
                }
                hexString = stringBuilder.ToString();
            }

            return hexString;
        }

        private void txtSecret_TextChanged(object sender, EventArgs e)
        {
            tmrGetHotpByChange.Stop();
            tmrGetHotpByChange.Start();
        }

        private void txtCounter_TextChanged(object sender, EventArgs e)
        {
            tmrGetHotp.Stop();

            tmrGetHotpByChange.Stop();
            tmrGetHotpByChange.Start();
        }

        private void btnRegenSecret_Click(object sender, EventArgs e)
        {
            // generate a random secret bytes
            new Random().NextBytes(secretKeyBytes);
            txtSecret.Text = ConvertBytesToHex(secretKeyBytes);

            GetHotp();
        }

        private void tmrGetHotpByChange_Tick(object sender, EventArgs e)
        {

            timerCounter = 0;

            tmrGetHotp.Start();

            tmrGetHotpByChange.Stop();

        }

        private void tmrGetHotp_Tick(object sender, EventArgs e)
        {
            if (++timerCounter < 11)
            {
                Console.WriteLine(timerCounter.ToString());
                return;
            }

            GetHotp();

            timerCounter = 0;
        }
    }
}
