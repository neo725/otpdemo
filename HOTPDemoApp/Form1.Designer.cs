﻿namespace HOTPDemoApp
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtCounter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSecret = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tmrGetHotpByChange = new System.Windows.Forms.Timer(this.components);
            this.tmrGetHotp = new System.Windows.Forms.Timer(this.components);
            this.btnRegenSecret = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtCounter
            // 
            this.txtCounter.Location = new System.Drawing.Point(139, 109);
            this.txtCounter.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtCounter.Name = "txtCounter";
            this.txtCounter.Size = new System.Drawing.Size(124, 32);
            this.txtCounter.TabIndex = 0;
            this.txtCounter.Text = "1";
            this.txtCounter.TextChanged += new System.EventHandler(this.txtCounter_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(43, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 24);
            this.label1.TabIndex = 1;
            this.label1.Text = "Counter";
            // 
            // txtSecret
            // 
            this.txtSecret.Location = new System.Drawing.Point(139, 56);
            this.txtSecret.Margin = new System.Windows.Forms.Padding(5);
            this.txtSecret.Name = "txtSecret";
            this.txtSecret.Size = new System.Drawing.Size(306, 32);
            this.txtSecret.TabIndex = 3;
            this.txtSecret.TextChanged += new System.EventHandler(this.txtSecret_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 56);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Secret";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tmrGetHotpByChange
            // 
            this.tmrGetHotpByChange.Interval = 3000;
            this.tmrGetHotpByChange.Tick += new System.EventHandler(this.tmrGetHotpByChange_Tick);
            // 
            // tmrGetHotp
            // 
            this.tmrGetHotp.Tick += new System.EventHandler(this.tmrGetHotp_Tick);
            // 
            // btnRegenSecret
            // 
            this.btnRegenSecret.Location = new System.Drawing.Point(473, 46);
            this.btnRegenSecret.Name = "btnRegenSecret";
            this.btnRegenSecret.Size = new System.Drawing.Size(158, 50);
            this.btnRegenSecret.TabIndex = 4;
            this.btnRegenSecret.Text = "RegenSecret";
            this.btnRegenSecret.UseVisualStyleBackColor = true;
            this.btnRegenSecret.Click += new System.EventHandler(this.btnRegenSecret_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(43, 170);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(515, 24);
            this.label3.TabIndex = 5;
            this.label3.Text = "The HOTP value and countdown will show in output window";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(699, 228);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnRegenSecret);
            this.Controls.Add(this.txtSecret);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCounter);
            this.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCounter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSecret;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer tmrGetHotpByChange;
        private System.Windows.Forms.Timer tmrGetHotp;
        private System.Windows.Forms.Button btnRegenSecret;
        private System.Windows.Forms.Label label3;
    }
}

