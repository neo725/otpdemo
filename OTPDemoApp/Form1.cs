﻿using System.Windows.Forms.VisualStyles;
using Base32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using System.Windows.Threading;
using OtpSharp;

namespace OTPDemoApp
{
    public partial class Form1 : Form
    {
        private DispatcherTimer timer;
        private byte[] secretKey;
        private readonly int timeShiftSeconds = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            secretKey = new byte[] { 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x21, 0xDE, 0xAD, 0xBE, 0xEF };

            InitialLayout();
        }

        /// <summary>
        /// 初始化控制項
        /// </summary>
        private void InitialLayout()
        {
            txtTotpCode.Text = String.Empty;
            lblSecondsToGo.Text = String.Empty;
            txtSecret.Text = String.Empty;
            picboxQRCode.Hide();
            lblQRCodeMask.Text = String.Empty;
            lblQRCodeMask.Hide();

            GetTotpCode();
        }

        /// <summary>
        /// 取得 Time-based One-Time Password
        /// </summary>
        private void GetTotpCode()
        {
            // OTPSharp.TOTP doc :
            // https://bitbucket.org/devinmartin/otp-sharp/wiki/TOTP

            var totp = new OtpSharp.Totp(secretKey);

            txtSecret.Text = ConvertBytesToHex(secretKey);

            var totpCode = totp.ComputeTotp(DateTime.UtcNow.AddSeconds(timeShiftSeconds));

            if (totpCode == txtTotpCode.Text)
            {
                return;
            }

            txtTotpCode.Text = totpCode;

            if (timer == null)
            {
                timer = new DispatcherTimer();
                timer.Interval = TimeSpan.FromMilliseconds(100);
                timer.Tick += (obj, evtargs) =>
                {
                    var remainingTime = totp.RemainingSeconds(DateTime.UtcNow.AddSeconds(timeShiftSeconds));
                    var countdown = 30 - remainingTime;

                    lblSecondsToGo.Text = remainingTime.ToString();

                    txtTotpCode.ForeColor = Color.Black;
                    if (countdown == 1)
                    {
                        GetTotpCode();
                        txtTotpCode.ForeColor = Color.Blue;
                    }
                    else if (remainingTime > 0 && remainingTime < 5)
                    {
                        txtTotpCode.ForeColor = Color.Red;
                    }
                    else if (remainingTime > 25)
                    {
                        txtTotpCode.ForeColor = Color.Blue;
                    }
                    
                };
                timer.IsEnabled = true;
            }
        }

        /// <summary>
        /// 轉換位元組資料為 16 進位字串表示
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private string ConvertBytesToHex(byte[] bytes)
        {
            string hexString = string.Empty;
            if (bytes != null)
            {
                var stringBuilder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    stringBuilder.Append(bytes[i].ToString("X2"));
                }
                hexString = stringBuilder.ToString();
            }

            return hexString;
        }

        private void ResetQRCodeTimer()
        {
            if (txtSecret.Text == String.Empty || txtAccount.Text == String.Empty)
            {
                lblQRCodeMask.Text = String.Empty;
                lblQRCodeMask.Hide();
                return;
            }

            tmrQRCode.Stop();
            picboxQRCode.Hide();
            tmrQRCode.Interval = 3000;
            tmrQRCode.Start();

            lblQRCodeMask.Text = "waiting load...";
            lblQRCodeMask.Show();
        }

        private string GenerateGoogleQRCode(string issuer, string account)
        {
            // https://code.google.com/p/google-authenticator/wiki/KeyUriFormat
            var base32Secret = Base32.Base32Encoder.Encode(secretKey);

            var otpauth = String.Format("otpauth://totp/{0}?secret={1}&issuer={2}",
                account,
                base32Secret,
                issuer);
            otpauth = HttpUtility.UrlEncode(otpauth);

            return
                String.Format("https://www.google.com/chart?chs=128x128&chld=M|0&cht=qr&chl={0}",
                    otpauth);
        }

        private void btnRegenSecret_Click(object sender, EventArgs e)
        {
            // regen a new secret can ref:
            // http://thegreyblog.blogspot.tw/2011/12/google-authenticator-using-it-in-your.html?q=google+authenticator
            // Generating the Secret Key
            new Random().NextBytes(secretKey);

            txtSecret.Text = ConvertBytesToHex(secretKey);
        }

        private void txtAccount_TextChanged(object sender, EventArgs e)
        {
            ResetQRCodeTimer();
        }

        private void txtSecret_TextChanged(object sender, EventArgs e)
        {
            ResetQRCodeTimer();
        }

        private void tmrQRCode_Tick(object sender, EventArgs e)
        {
            lblQRCodeMask.Text = "waiting load QRCode...";
            lblQRCodeMask.Show();

            tmrQRCode.Stop();

            var url = GenerateGoogleQRCode("OTPDemo", txtAccount.Text.Trim().ToLower());

            lblQRCodeMask.Hide();
            picboxQRCode.Load(url);
            picboxQRCode.Show();
        }
    }
}
