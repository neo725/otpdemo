﻿namespace OTPDemoApp
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSecret = new System.Windows.Forms.TextBox();
            this.btnRegenSecret = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblSecondsToGo = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTotpCode = new System.Windows.Forms.TextBox();
            this.picboxQRCode = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAccount = new System.Windows.Forms.TextBox();
            this.tmrQRCode = new System.Windows.Forms.Timer(this.components);
            this.lblQRCodeMask = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picboxQRCode)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(103, 61);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Secret";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSecret
            // 
            this.txtSecret.Location = new System.Drawing.Point(187, 61);
            this.txtSecret.Margin = new System.Windows.Forms.Padding(5);
            this.txtSecret.Name = "txtSecret";
            this.txtSecret.Size = new System.Drawing.Size(552, 30);
            this.txtSecret.TabIndex = 1;
            this.txtSecret.TextChanged += new System.EventHandler(this.txtSecret_TextChanged);
            // 
            // btnRegenSecret
            // 
            this.btnRegenSecret.Location = new System.Drawing.Point(768, 52);
            this.btnRegenSecret.Name = "btnRegenSecret";
            this.btnRegenSecret.Size = new System.Drawing.Size(158, 50);
            this.btnRegenSecret.TabIndex = 2;
            this.btnRegenSecret.Text = "RegenSecret";
            this.btnRegenSecret.UseVisualStyleBackColor = true;
            this.btnRegenSecret.Click += new System.EventHandler(this.btnRegenSecret_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 23);
            this.label2.TabIndex = 3;
            this.label2.Text = "Seconds to go";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSecondsToGo
            // 
            this.lblSecondsToGo.AutoSize = true;
            this.lblSecondsToGo.ForeColor = System.Drawing.Color.Blue;
            this.lblSecondsToGo.Location = new System.Drawing.Point(184, 111);
            this.lblSecondsToGo.Name = "lblSecondsToGo";
            this.lblSecondsToGo.Size = new System.Drawing.Size(20, 23);
            this.lblSecondsToGo.TabIndex = 4;
            this.lblSecondsToGo.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(65, 163);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 23);
            this.label3.TabIndex = 5;
            this.label3.Text = "Your OTP is";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTotpCode
            // 
            this.txtTotpCode.Location = new System.Drawing.Point(187, 163);
            this.txtTotpCode.Name = "txtTotpCode";
            this.txtTotpCode.Size = new System.Drawing.Size(138, 30);
            this.txtTotpCode.TabIndex = 6;
            this.txtTotpCode.Text = "999999";
            this.txtTotpCode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // picboxQRCode
            // 
            this.picboxQRCode.Location = new System.Drawing.Point(187, 272);
            this.picboxQRCode.Name = "picboxQRCode";
            this.picboxQRCode.Size = new System.Drawing.Size(128, 128);
            this.picboxQRCode.TabIndex = 7;
            this.picboxQRCode.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(89, 219);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 23);
            this.label4.TabIndex = 8;
            this.label4.Text = "Account";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAccount
            // 
            this.txtAccount.Location = new System.Drawing.Point(187, 216);
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(510, 30);
            this.txtAccount.TabIndex = 9;
            this.txtAccount.TextChanged += new System.EventHandler(this.txtAccount_TextChanged);
            // 
            // tmrQRCode
            // 
            this.tmrQRCode.Tick += new System.EventHandler(this.tmrQRCode_Tick);
            // 
            // lblQRCodeMask
            // 
            this.lblQRCodeMask.BackColor = System.Drawing.Color.Gainsboro;
            this.lblQRCodeMask.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQRCodeMask.Location = new System.Drawing.Point(198, 318);
            this.lblQRCodeMask.Name = "lblQRCodeMask";
            this.lblQRCodeMask.Size = new System.Drawing.Size(107, 31);
            this.lblQRCodeMask.TabIndex = 10;
            this.lblQRCodeMask.Text = "label5";
            this.lblQRCodeMask.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 22F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(998, 448);
            this.Controls.Add(this.lblQRCodeMask);
            this.Controls.Add(this.txtAccount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.picboxQRCode);
            this.Controls.Add(this.txtTotpCode);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblSecondsToGo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnRegenSecret);
            this.Controls.Add(this.txtSecret);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Calibri", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "Form1";
            this.Text = "OTP Demo";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picboxQRCode)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSecret;
        private System.Windows.Forms.Button btnRegenSecret;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblSecondsToGo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTotpCode;
        private System.Windows.Forms.PictureBox picboxQRCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAccount;
        private System.Windows.Forms.Timer tmrQRCode;
        private System.Windows.Forms.Label lblQRCodeMask;
    }
}

