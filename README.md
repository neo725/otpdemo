# README #

This source code is developed by Microsoft Virtual Studio 2013 in C#.

here is simple description of project:

1. HOTPDemoApp -
the HOTP showcase in app mode.

2. OTPDemo -
the TOTP showcase in app mode. just the algorithm is coded in project.

3. OTPDemoApp -
the TOTP showcase in app mode, which is use OTPSharp ([https://bitbucket.org/devinmartin/otp-sharp/wiki/Home](https://bitbucket.org/devinmartin/otp-sharp/wiki/Home)) library.

4. OTPDemoWeb -
the TOTP input website, it show about verify demostration.