﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Web;
using System.Windows.Media;
using System.Windows.Threading;

namespace OTPDemo
{
    public class ViewModel : INotifyPropertyChanged
    {
        private int _secondsToGo;
        public int SecondsToGo
        {
            get { return _secondsToGo; }
            private set { _secondsToGo = value; OnPropertyChanged("SecondsToGo"); if (SecondsToGo == 30 - 1) CalculateOneTimePassword(); }
        }


        private string _identity;
        public string Identity
        {
            get { return _identity; }
            set { _identity = value; OnPropertyChanged("Identity"); OnPropertyChanged("QRCodeUrl"); CalculateOneTimePassword(); }
        }

        private string _issuer;
        public string Issuer
        {
            get { return _issuer; }
            set { _issuer = value; OnPropertyChanged("Issuer"); OnPropertyChanged("QRCodeUrl"); CalculateOneTimePassword(); }
        }

        private byte[] _secret;
        public byte[] Secret
        {
            get { return _secret; }
            set { _secret = value; OnPropertyChanged("Secret"); OnPropertyChanged("QRCodeUrl"); CalculateOneTimePassword(); }
        }

        public string QRCodeUrl
        {
            get { return GetQRCodeUrl(); }
        }

        private Int64 _timestamp;

        public Int64 Timestamp
        {
            get { return _timestamp; }
            private set { _timestamp = value; OnPropertyChanged("Timestamp"); }
        }

        private byte[] _hmac;

        public byte[] Hmac
        {
            get { return _hmac; }
            private set { _hmac = value; OnPropertyChanged("Hmac"); OnPropertyChanged("HmacPart1"); OnPropertyChanged("HmacPart2"); OnPropertyChanged("HmacPart3"); }
        }

        public byte[] HmacPart1
        {
            get { return _hmac.Take(Offset).ToArray(); }
        }

        public byte[] HmacPart2
        {
            get { return _hmac.Skip(Offset).Take(4).ToArray(); }
        }

        public byte[] HmacPart3
        {
            get { return _hmac.Skip(Offset + 4).ToArray(); }
        }

        private int _offset;
        public int Offset
        {
            get { return _offset; }
            private set { _offset = value; OnPropertyChanged("Offset"); }
        }

        private string _oneTimePassword;
        public string OneTimePassword
        {
            get { return _oneTimePassword.PadLeft(6, '0'); }
            set { _oneTimePassword = value; OnPropertyChanged("OneTimePassword"); }
        }

        private string GetQRCodeUrl()
        {
            // https://code.google.com/p/google-authenticator/wiki/KeyUriFormat
            var base32Secret = Base32.Encode(Secret);

            var otpauth = String.Format("otpauth://totp/{0}?secret={1}&issuer={2}",
                Identity,
                base32Secret,
                Issuer);
            otpauth = HttpUtility.UrlEncode(otpauth);

            return
                String.Format("https://www.google.com/chart?chs=128x128&chld=M|0&cht=qr&chl={0}",
                    otpauth);
        }

        private void CalculateOneTimePassword()
        {
            // https://tools.ietf.org/html/rfc4226
            // TC = Unixtime(now - T0) / TS
            Timestamp = Convert.ToInt64(GetUnixTimestamp() / 30);

            // TOTP = HOTP(Secret, TC)
            var data = BitConverter.GetBytes(Timestamp).Reverse().ToArray();
            Hmac = new HMACSHA1(Secret).ComputeHash(data);
            Offset = Hmac.Last() & 0x0F;

            var totp =
                ((Hmac[Offset + 0] & 0x7f) << 24) |
                ((Hmac[Offset + 1] & 0xff) << 16) |
                ((Hmac[Offset + 2] & 0xff) << 8) |
                (Hmac[Offset + 3] & 0xff);

            var totpValue = totp % 1000000;

            OneTimePassword = totpValue.ToString();
        }

        private static Int64 GetUnixTimestamp()
        {
            return Convert.ToInt64(Math.Round((DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0)).TotalSeconds));
            //var dateUtcNow = DateTime.UtcNow;
            //var date0 = new DateTime(1970, 1, 1, 0, 0, 0);

            //var timestamp = Convert.ToInt64(Math.Round((dateUtcNow - date0).TotalSeconds));

            //return timestamp;
        }

        public ViewModel()
        {
            var timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(100);
            timer.Tick += (s, e) =>
            {
                SecondsToGo = 30 - Convert.ToInt32(GetUnixTimestamp() % 30);
            };
            timer.IsEnabled = true;

            Secret = new byte[] { 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x21, 0xDE, 0xAD, 0xBE, 0xEF };
            //Secret = new byte[] { 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x21, 0xDE, 0xAD, 0xBE, 0xEF, 0x9D, 0x38 };
            Identity = "user@host.com";

            Issuer = "MyDemoOTPAuth";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}
