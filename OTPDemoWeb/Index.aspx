﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="OTPDemoWeb.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            Two-Step Verification Key
        </div>
        <div>
            <asp:TextBox ID="txtKey" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:Button ID="btnSubmit" runat="server" Text="Verify" UseSubmitBehavior="True" OnClick="btnSubmit_Click" />
        </div>
        <div>
            <asp:Label ID="lblPassed" runat="server" Text="Unknow"></asp:Label>
        </div>
    </form>
</body>
</html>
