﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OtpSharp;

namespace OTPDemoWeb
{
    public partial class Index : System.Web.UI.Page
    {
        private byte[] secretKey;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            secretKey = new byte[] {0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x21, 0xDE, 0xAD, 0xBE, 0xEF};

            var totp = new Totp(secretKey);

            long timeStepMatched = 0;

            var passed = totp.VerifyTotp(txtKey.Text, out timeStepMatched);

            if (passed == false)
            {
                lblPassed.Text = "False";
                lblPassed.ForeColor = Color.Red;
                return;
            }

            lblPassed.Text = "True";
            lblPassed.ForeColor = Color.Blue;

        }
    }
}